//283. Move Zeroes

//very important inplace shift of non zero element to left using partition algo very imp

/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var moveZeroes = function(nums) {
    if(nums.length<2) return nums;
       let left = 0;
       for(let right = 0; right<nums.length;right++){
           if(nums[right] != 0){
               let temp = nums[right];
               nums[right] = nums[left];
               nums[left] = temp;
               left++
           }
           
       }
   };
