//Input: nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], target = 9
//Output: [(0, 7), (1, 6), (2, 5), (3, 4)]
//Explanation: The elements at indices (0, 7), (1, 6), (2, 5), and (3, 4) form pairs with sums equal to the target 9: (1 + 8), (2 + 7), (3 + 6), and (4 + 5).


//Input: nums = [-3, -1, 0, 2, 3, 6, 7, 8], target = 9
//Output: [(2, 7), (3, 6)]
//Explanation: The elements at indices (2, 7) and (3, 6) form pairs with sums equal to the target 9: (0 + 9) and (2 + 7).

const allPairsSummingToTarget = (nums,target)=>{
    let first = 0;
    let last = nums.length - 1;
    let result = [];
    while(first<last){
        if((nums[first]+nums[last]) === target){
            result.push([first,last]);
            first++;
            last--;
        }else if((nums[first]+nums[last]) < target){
            first++;
        }else if((nums[first]+nums[last]) > target){
            last--;
        }
    }
    return result;
};

console.log(allPairsSummingToTarget([1, 2, 3, 4, 5, 6, 7, 8, 9, 10],9));
//[ [ 0, 7 ], [ 1, 6 ], [ 2, 5 ], [ 3, 4 ] ]
console.log(allPairsSummingToTarget([-3, -1, 0, 2, 3, 6, 7, 8],9));
//[ [ 3, 6 ], [ 4, 5 ] ]