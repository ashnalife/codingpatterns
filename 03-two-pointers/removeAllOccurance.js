//Input: nums = [3, 2, 3, 5, 3, 7, 8, 3], val = 3
//Output: 4
//Explanation: After removing all occurrences of 3 from the array, the modified array becomes [2, 5, 7, 8], and its length is 4.

//here the code has to be in-place and insertion order has to preserved 
const removeAllOccurances = (arr,val) => {
    //improvement inorder to make inplace i am doing array suffling moving target element right using 2 pointer.
    let left = 0;
    let right = arr.length-1;

    while(left < right){
        if(arr[left]===val){
            if(arr[right]===val){
                right--;
            }else{
                let temp = arr[right];
                arr[right] = arr[left];
                arr[left] = temp;
                right--;
                left++;
            }
        }else{
            if(arr[right]===val){
                right--;
            }else{
                left++;
            }
        }
    }
}
let arr = [3, 2, 3, 5, 3, 7, 8, 3];
removeAllOccurances(arr,3);
console.log(arr);

//op  [8, 2, 7, 5,3, 3, 3, 3]