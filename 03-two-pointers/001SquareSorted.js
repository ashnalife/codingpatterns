//977. Squares of a Sorted Array


/**
 * @param {number[]} nums
 * @return {number[]}
 */
var sortedSquares = function(nums) {
    let sortedSqArray = [];
    nums=nums.map(val=>val*val)
    let left = 0;
    let right = nums.length-1;

    while(left<=right){
        if(left===right){
            sortedSqArray.unshift(nums[left]);
            break;
        }

        if(nums[left]>nums[right]){
            sortedSqArray.unshift(nums[left]);
            left++;
        }else{
            sortedSqArray.unshift(nums[right]);
            right--;
        }
    }
    return sortedSqArray;
};