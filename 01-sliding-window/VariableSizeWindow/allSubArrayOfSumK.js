/**
 * 
 * @param {[number]} array 
 * @param {Number} sum 
 * @returns {[[Number]]}
 * @description This function will return all sub arrays of variable length of sum "sum" from an an array "array"
 * @throws Error when param array is not array or empty array and when sum is not passed as argument.
 * @example allSubArrayOfSumK([4, 1, 1, 1, 2, 3, 5], 5)
 */
const allSubArrayOfSumK = (array, sum) => {
  if (!Array.isArray(array))
    throw new Error("Expected array[Number] as the first argument");
  if (array.length == 0) throw new Error("Empty Array in Args");
  if (!sum) throw new Error("Expected 2nd arguent as number type");

  let tailPointer = 0;
  let headPointer = 0;
  let currentSubArraySum = 0;
  let lengthsOfSumK = [];

  while (headPointer < array.length) {
    currentSubArraySum += array[headPointer];
    if (currentSubArraySum > sum) {
      while (currentSubArraySum > sum) {
        currentSubArraySum -= array[tailPointer];
        tailPointer++;
      }
      if (currentSubArraySum == sum) {
        lengthsOfSumK.push(array.slice(tailPointer, headPointer + 1));
      }
    } else if (currentSubArraySum == sum) {
      lengthsOfSumK.push(array.slice(tailPointer, headPointer + 1));
    }
    headPointer++;
  }
  return lengthsOfSumK;
};

console.log(allSubArrayOfSumK([4, 1, 1, 1, 2, 3, 5], 5));
//Enhance this code for -ve imputs
//console.log(allSubArrayOfSumK([-4, 1,1, 1, 2, -3, 5], 1));
