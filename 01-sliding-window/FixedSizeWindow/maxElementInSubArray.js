//Given an array of size n and a window size k.
//find an array of numbers where each number is the maximum of subarray.

//ex  [1,3,-1,-3,5,3,6,7] window size 3
//op shoud be [3,3,5,5,6,7]

/**
 *
 * @param {[number]} numArray
 * @param {number} windowSize
 * @returns {[number]}
 * @description method uses sliding window technique to find array of all element maximum in sub arrays.
 */
const getAllMaxInSubArrays = (numArray, windowSize) => {
  if (numArray.length == 0 || !Array.isArray(numArray)) {
    throw new TypeError("Invalid Argument");
  }
  let i = 0;
  let j = 0;
  const result = [];
  let localMax = [];
  localMax[0] = numArray[0];
  while (j < numArray.length) {
    //maintain dequeue for second max third max etc
    if (numArray[j] > localMax[0]) {
      localMax = [];
      localMax[0] = numArray[j];
    } else {
      localMax.push(numArray[j]);
    }
    if (j - i + 1 < windowSize) {
      j++;
    } else if (j - i + 1 == windowSize) {
      //first window ends and successive windows will execute this block only
      result.push(localMax[0]);
      if (numArray[i] == localMax[0]) {
        localMax.shift();
      }
      i++;
      j++;
    }
  }
  return result;
};

console.log(getAllMaxInSubArrays([1, 3, -1, -3, 5, 3, 6, 7], 3));

