//1343
// Given an array of integers arr and two integers k and threshold, return the number of sub-arrays of size k and average greater than or equal to threshold.

 

// Example 1:

// Input: arr = [2,2,2,2,5,5,5,8], k = 3, threshold = 4
// Output: 3
// Explanation: Sub-arrays [2,5,5],[5,5,5] and [5,5,8] have averages 4, 5 and 6 respectively. All other sub-arrays of size 3 have averages less than 4 (the threshold).

// Example 2:

// Input: arr = [11,13,17,23,29,31,7,5,2,3], k = 3, threshold = 5
// Output: 6
// Explanation: The first 6 sub-arrays of size 3 have averages greater than 5. Note that averages are not integers.


/**
 * @param {number[]} arr
 * @param {number} k
 * @param {number} threshold
 * @return {number}
 */
var numOfSubarrays = function(arr, k, threshold) {
    let headPointer = 0;
    let tailPointer = 0;
    let localSum = 0;
    let noOfSubArrayOverThreshold = 0;
    while(headPointer < arr.length){
        localSum += arr[headPointer];
        if(headPointer-tailPointer+1 < k){
            headPointer++;
        }else if(headPointer-tailPointer+1 == k){
            if(Math.floor(localSum/k)>=threshold){
                noOfSubArrayOverThreshold++;
            }
            localSum -= arr[tailPointer];
            headPointer++;
            tailPointer++;
        }
    }
    return noOfSubArrayOverThreshold;
};

numOfSubarrays([2,2,2,2,5,5,5,8],3,4);