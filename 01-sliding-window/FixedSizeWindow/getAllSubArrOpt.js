// get all possible sub array optimised by using sliding window technique.

/**
 *
 * @description this function takes integer array of which sub array has to be calculated.
 * @param {[number]} array 
 * @param {number} k is the size of window.
 * @returns {[[number]]}
 */
const getAllSubArray = (array,k) => {
  let i = 0;
  let j = 0;//pointer initialisation

  let result = [];
  let currResult = [];//result array and temp array initialisation

  while (j < arr.length) { //SW technique implimentation O(n)
    currResult.push(array[j]);
    if (j - i + 1 < k) { // this parts executes till first window created.
      j++;
    } else if (j - i + 1 == k) { // this is the heart of algo where optimisation happens
      result.push([...currResult]);
      currResult = currResult.slice(1, k);
      j++;
      i++;
    }
  }
  return result;
};

let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];// test input.

console.log(getAllSubArray(arr,4));

///op
// [
//   [1, 2, 3, 4],
//   [2, 3, 4, 5],
//   [3, 4, 5, 6],
//   [4, 5, 6, 7],
//   [5, 6, 7, 8],
//   [6, 7, 8, 9],
// ];
