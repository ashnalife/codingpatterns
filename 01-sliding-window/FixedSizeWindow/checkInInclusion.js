
/**
 * @param {string} s1
 * @param {string} s2
 * @return {boolean}
 */
var checkInclusion = function(s1, s2) {
    s1=s1.split("").sort().join("");
    let hp = 0;
    let tp = 0;

    while(hp<s2.length){
      if(hp-tp+1<s1.length){
        hp++;
      }else if(hp-tp+1==s1.length){
        if(s2.substring(tp,hp+1).split("").sort().join("")==s1){
          return true;
        }
        hp++;
        tp++;
      }
    }
    return false;
};

console.log(checkInclusion("ab","eidbaooo"));