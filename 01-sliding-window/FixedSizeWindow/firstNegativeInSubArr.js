/**
 *
 * @param {[Number]} arr
 * @param {Number} k
 */
const firstNegativesInSubArray = (arr, k) => {
  let i = 0;
  let j = 0;
  const result = [];
  const negativesInWindow = [];
  while (j < arr.length) {
    if (arr[j] < 0) negativesInWindow.push(arr[j]);
    //first window.
    if (j - i + 1 < k) {
      j++;
    } else if (j - i + 1 == k) {
      //successive window
      if (negativesInWindow.length === 0) {
        result.push("X");
      } else {
        if (arr[i] === negativesInWindow[0]) {
          result.push(negativesInWindow[0]);
          negativesInWindow.shift();
        } else {
          result.push(negativesInWindow[0]);
        }
      }

      i++;
      j++;
    }
  }
  return result;
};

let test = [-8, 2, -3, 6, 10, -12, 7, -8, -9, 0, -16];
let k = 3;
//op [-8,  -3, -3, -12,-12, -12, -8,  -8,-9]
console.log(firstNegativesInSubArray(test, k));
