/**
 * @description maxSumSubArraySizeK find the maximum sum produced by subArray amongs all subarray of size k;
 * @param {number} array
 * @param {number} k
 * @returns {number}
 */
const maxSumSubArraySizeK = (array, k) => {
  let i = 0;
  let j = 0;

  let sum = -Infinity;
  let locSum = 0;
  while (j < array.length) {
    locSum += array[j];
    if ((j - i + 1) < k) {
      j++;
    } else if ((j - i + 1 ) == k) {
      sum = Math.max(sum, locSum);
      locSum = locSum - array[i];
      i++;
      j++;
    }
  }
  return sum;
};

//test inputs
let arr1 = [100, 200, 300, 400];
let k1 = 2;
//op 700
let arr2 = [1, 4, 2, 10, 23, 3, 1, 0, 20];
let k2 = 4;
//op 39
let arr3 = [100, 200, 300, 400];
let k3 = 2;
//op 700

let arr4 = [100, -200, 300, -400];

console.log(maxSumSubArraySizeK(arr4, k3));
