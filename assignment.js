/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var targetIndices = function(nums, target) {
     //Space = O(n)
     //Time = O(nlog n)
 
     nums = nums.sort((a, b) => a - b);
 
     let map = {};
 
     for(let i = 0; i < nums.length; i++){
         if(map[nums[i]]=== undefined){
            map[nums[i]] = []; 
         }
         map[nums[i]].push(i);
     }
 
     return map[target] ? map[target] : [];
     
 };