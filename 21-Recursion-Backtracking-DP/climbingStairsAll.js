let times = 0;

console.log("start", new Date());
function climb(n, sl, s) {
  if (n < 0) {
    return ;
  } else if (n === 0) {
    s.push([...sl]);
  } else {
    sl.push("1");
    climb(n - 1, sl, s);
    sl.pop();

    sl.push("2");
    climb(n - 2, sl, s);
    sl.pop();
  }
}
const s = [];
climb(5, [], s);
console.log(s);
console.log("end", new Date());
