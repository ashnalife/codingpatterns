/**
 * @param {number} n
 * @return {number}
 */
var climbStairs = function(n) {

    if(n==0){
        return 1;
    }
    if(n<0){
        //invalid input
        return 0;
    }


    return climbStairs(n-1) + climbStairs(n-2)
    
};