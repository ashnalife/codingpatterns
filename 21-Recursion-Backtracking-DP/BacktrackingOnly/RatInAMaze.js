//Problem link GFG
//https://www.geeksforgeeks.org/problems/rat-in-a-maze-problem/1?utm_source=geeksforgeeks&utm_medium=ml_article_practice_tab&utm_campaign=article_practice_tab
function findPath(m,n){
    //create isVisitedMatrix
    if(!findPath.isVisited){
        findPath.isVisited = [];
        for(let i = 0; i<n; i++){
            findPath.isVisited[i] = new Array(n).fill(false);
        }
    }

    let result = [];
    let localResult = "";
    pathGenerator(m,0,0,localResult,result);

    findPath.isVisited = undefined;
    return result;
}

/**
 * 
 * @param {[[Number]]} m ref of 2D array
 * @param {Number} x starting point for the traversal in m
 * @param {Number} y starting point in m[x]
 * @param {String} localResult for caching lical result initially empty 
 * @param {[String]} result all possible path for a rat to reach the last cell .
 */
function pathGenerator(m,x,y,localResult,result){
    //This is the cell where Rat cant got if zero at matrix cell visited or not valid cell or not all at one place.
    if(x<0 || y<0 || x>=m.length || y>=m[0].length || findPath.isVisited[x][y]||m[x][y]===0){
        localResult=localResult.substring(0,localResult.length-1);
        return ;
    }
    if(x===m.length-1 && y===m[0].length-1){
        result.push(localResult);
        localResult=localResult.substring(0,localResult.length-1);
    }

//we have 4 options [Up,Right,Down,Left] lexicographically its DLRU so call prioritization should be
    //accordingly
    findPath.isVisited[x][y] = true;
    pathGenerator(m,x+1,y,localResult+"D",result);
    pathGenerator(m,x,y-1,localResult+"L",result);
    pathGenerator(m,x,y+1,localResult+"R",result);
    pathGenerator(m,x-1,y,localResult+"U",result);

    //Visited ko clean bhi karna he because all paths nikalna he overlapping bhi ho sakti he.
    findPath.isVisited[x][y] = false;

}


let maze = [
  [1, 0, 0, 0],
  [1, 1, 0, 1],
  [1, 1, 0, 0],
  [0, 1, 1, 1],
];

let r = findPath(maze,4);
console.log(r);

