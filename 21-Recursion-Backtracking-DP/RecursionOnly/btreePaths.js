/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {string[]}
 */
var binaryTreePaths = function(root) {
    let result = [];
    let local = "";

    createAllBinaryPath(root,local,result);

    return result;
};

function createAllBinaryPath(root,local,result){
    let k = local === "" ? ""+root.val : local+"->"+root.val
    if(root.left===null && root.right===null){
        result.push(k);
        return
    }
    if(root.left!==null){
        createAllBinaryPath(root.left,k,result);
    }
    if(root.right!==null){
        createAllBinaryPath(root.right,k,result);
    }
}