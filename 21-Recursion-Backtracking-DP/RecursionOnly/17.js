//https://leetcode.com/problems/letter-combinations-of-a-phone-number/


var letterCombinations = function (digits) {
    if(digits===""){
        return [];
    }  
    let dialPad = {
      2: ["a", "b", "c"],
      3: ["d", "e", "f"],
      4: ["g", "h", "i"],
      5: ["j", "k", "l"],
      6: ["m", "n", "o"],
      7: ["p", "q", "r", "s"],
      8: ["t", "u", "v"],
      9: ["w", "x", "y", "z"],
    };
    let result = [];
    let local = "";
    generateLetterCombinationRec(digits, local, result, dialPad);
    return result;
  };
  
  function generateLetterCombinationRec(digits, local, result, dialPad) {
    if (digits.length === 0) {
      result.push(local);
      return;
    }
  
    dialPad[digits.split("")[0]].map((alphabet, index) => {
      local += alphabet;
      generateLetterCombinationRec(
        digits.slice(1, digits.length),
        local,
        result,
        dialPad
      );
      local = local.substring(0, local.length - 1);
    });
  }

