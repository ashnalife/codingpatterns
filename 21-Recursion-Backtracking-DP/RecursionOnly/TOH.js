/**
 * 
 * @param {Number} plates 
 * @param {String} source 
 * @param {String} destination 
 * @param {String} auxilary 
 */
function TOH(plates,source,destination,auxilary){
    //base condition.
    if(plates===0){
        return ;
    }
    //hypothesis
    TOH(plates-1,source,auxilary,destination);
    //induction stem he print karna.
    console.log("Moving plate",plates,"from",source,"to",destination);
    //hypothesis
    TOH(plates-1,auxilary,source,destination);
}

TOH(3,"SOURCE","DESTINATION","AUXILARY")