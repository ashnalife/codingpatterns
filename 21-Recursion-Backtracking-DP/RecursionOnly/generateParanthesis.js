var getRecursiveParam = (result,open,close,n,str)=>{
    //console.log(result,open,close,n,str);
    if(str===undefined){
        return;
    }
    if(str.length>2*n || open>n || close>n){
        return
    }
    if(close>open){
        return;
    }
    if(str.length === 2*n && open ===n && close===n){
        result.push(str);
        return;
    }
    //two choices
    getRecursiveParam(result,open+1,close,n,str+'(');
    getRecursiveParam(result,open,close+1,n,str+')');
    return;
}
var generateParenthesis = function(n) {
    var result = [];
    getRecursiveParam(result,0,0,n,'');
    return result;
};