/**
 * 
 * @param {[Number]} array 
 * @returns {Boolean}
 * @description This function test if given array is sorted in non-decreasing fashion or not.
 */
function isSortedNonDecreasing(array){
    //base condition
    if(array.length<2){
        return true;
    }
    //induction

    if(array[array.length-1]<array[array.length-2]){
        return false;
    }

    //hypothesis this function can test sortness
    return isSortedNonDecreasing(array.slice(0,array.length-1));
}

console.log(isSortedNonDecreasing([1,2,3,4,5,100,7,8,9]));