/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var subsets = function (nums) {
  nums = [...nums]; //do not mutate argument array.
  let result = [];
  let localResult = [];
  let generateSequence = (nums) => {
    //base condition.
    if (nums.length === 0) {
      result.push([...localResult]);
      return ;
    }

    //induction step
    //coice 1 dont choose left most element
      generateSequence(nums.slice(1, nums.length));

    //choice 2 choose left most element
        localResult.push(nums[0]);
      generateSequence(nums.slice(1, nums.length));

      //***************This is the main step */
      localResult.pop();
  };
  generateSequence(nums);
  return result;
};

let k = subsets([1, 2, 3]);
console.log(k);
