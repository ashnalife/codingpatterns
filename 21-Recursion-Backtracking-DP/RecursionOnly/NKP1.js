/**
 * 
 * @param {[Number]} array 
 * @param {Number}
 * @returns 
 */
function linearSearchRec(array,target){
//base condition.
    if(array.length===0){
        return -1;
    }
    if(array[array.length-1]===target){//induction step
        return array.length-1;
    }


    //Hypothesis is my function is able to find element.
    return linearSearchRec(array.slice(0,array.length-1),target);
}

console.log(linearSearchRec([1,2,3,4,5,6,7],0));