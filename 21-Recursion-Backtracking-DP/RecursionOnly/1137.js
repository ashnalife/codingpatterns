/**
 * @param {number} n
 * @return {number}
 */

let memo = []; 
var tribonacci = function(n) {
    if(n===0 || n===1){
        return n;
    }  
    if(n===2){
        return 1;
    } 
    if(memo[n]){
        return memo[n];
    }
    memo[n] =  tribonacci(n-3)+tribonacci(n-2)+tribonacci(n-1);

    return memo[n];
};