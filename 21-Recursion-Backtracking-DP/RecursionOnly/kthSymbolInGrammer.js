var kthGrammar = function(n, k) {
    let res = createNthWord(n);
    return res[k-1];
  };
  
  function createNthWord(n, flag) {
    //base condition
    if (n === 1) {
      return "0";
    }
    //hypothesis
    let prev = createNthWord(n - 1);
    let res = [];
    for (let i = 0; i < prev.length; i++) {
      if (prev[i] === '0') {
        res.push('01');
      } else {
        res.push('10');
      }
    }
    return res.join("");
  }

  //above solution giving TLE need to optimise on k

console.log(kthGrammar(2,2));