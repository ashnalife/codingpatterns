
function sortRecursion(array){
    if(array.length === 0){
        return []; 
    }
    if(array.length === 1){
        //an array of length 1 is sorted in itself
         return array;
    }


    //hypothesis
    //if I send a shorted array to same function it will give me result for smaller input.

    let sortedArrayTillPrevious = sortRecursion(array.slice(0,array.length-1));

    return putItemAtItsCorrectPosition(array[array.length-1],sortedArrayTillPrevious)

};

function putItemAtItsCorrectPosition(item, array) {
    //2,4,7
    let result = new Array(array.length+1).fill(-Infinity);
    let ifPlacefound = false;
    let ap = 0;
     result.forEach((temp,index)=>{
        if(array[ap]>item && !ifPlacefound){
            ifPlacefound = true;
            result[index]=item;
        }else{
            if(ap<array.length){
                result[index] = array[ap];
                ap++;
            }else{
                result[index]=item;
            }  
        }
    });
    return result;
}


let combined_test_input = [
    [4, 2, 7, 1, 9],
    [10, 5, 8, 3, 6],
    [1, 1, 1, 1, 1],
    [9, 6, 3, 8, 5],
    [2, 4, 6, 8, 10],
    [5, 3, 1, 7, 9],
    
    [1, 2, 3, 4, 5],
    [0, 2, 4, 6, 8],
    [11, 22, 33, 44, 55],
    
    [5, 5, 7, 8, 10],
    [2, 2, 2, 5, 5, 7],
    [1, 2, 2, 3, 3, 4],
    
    [5, -3, 0, 7, -1],
    [-10, 0, 5, -5, 10],
    [8, -2, 4, -6, 1],
    
    [3, 5, 1, 3, 2, 5],
    [8, 8, 3, 2, 3, 5],
    [1, 1, 2, 2, 3, 3],
    
    [],
    [42]
];



combined_test_input.map((testInput,index)=>{
    console.log(index,"------------",testInput,"-----------",sortRecursion(testInput));
});

//some tests are failing may be 9 or 10 check later.

