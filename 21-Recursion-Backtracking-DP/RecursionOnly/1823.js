/**
 * @param {number} n
 * @param {number} k
 * @return {number}
 */
var findTheWinnerHelper = function(candidates, k) {
    //base condition
    if(candidates.length===1){
        return candidates[0];
    }
    //hypothesis
    return findTheWinnerHelper(
        //induction step
        deleteIndexMoveNextToZero(candidates,k),k
        );
};

function deleteIndexMoveNextToZero(array,k){

    let result = [];
    k=k-1;
    let i = k%array.length;
    while(i!==(k%array.length-1)){
        result.push(array[i]);
        i = (i+1)%array.length;
    }
    return result;
}

findTheWinnerHelper([1,2,3,4,5,6],5);