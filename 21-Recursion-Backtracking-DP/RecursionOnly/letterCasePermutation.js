/**
 * @param {string} s
 * @return {string[]}
 */
var letterCasePermutation = function(s) {
    let result = [];
    let localRes = "";
    generateLetterCasePermutation(s,localRes,result);
    return result;
};

function generateLetterCasePermutation(s,localRes,result){

    if(s.length === 0){
        result.push(localRes);
        return 
    }

    // upper case
    if(s[0].charCodeAt(0)<=90 && s[0].charCodeAt(0)>=65){
        generateLetterCasePermutation(s.substring(1,s.length),localRes+s[0],result);
        generateLetterCasePermutation(s.substring(1,s.length),localRess[0].toLowerCase(),result);

    }else if(s[0].charCodeAt(0)>=97 && s[0].charCodeAt(0)<=122){
        //lowercase
        generateLetterCasePermutation(s.substring(1,s.length),localRes+s[0],result);
        generateLetterCasePermutation(s.substring(1,s.length),localRes+s[0].toUpperCase(),result);
    }else{
        //expecting number
        generateLetterCasePermutation(s.substring(1,s.length),localRes+s[0],result);
    }

}

letterCasePermutation("a1b2");