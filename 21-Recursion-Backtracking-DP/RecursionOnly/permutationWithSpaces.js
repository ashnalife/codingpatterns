/**
 *
 * @param {String} string
 * @returns
 */
function permutationHelper(string) {
  if (string.length < 2) {
    return string;
  }
  let result = [];

  function permutation(ip, op) {
    //base condition

    if (ip === "") {
      result.push(op);
      return;
    }

    //decision

    //1 take first character with space

    permutation(ip.substring(1, ip.length), op + " " + ip.substring(0, 1));

    //2 decision first character only

    permutation(ip.substring(1, ip.length), op + "" + ip.substring(0, 1));
  }
  permutation(string.substring(1, string.length), string.substring(0, 1));
  return result;
}

//https://www.geeksforgeeks.org/problems/permutation-with-spaces3627/1
//code submission sucessfull


