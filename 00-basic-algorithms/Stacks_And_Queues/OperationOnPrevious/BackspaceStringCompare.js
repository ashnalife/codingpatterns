/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var backspaceCompare = function(s, t) {
    let sStack = getStackOf(s);
    let tStack = getStackOf(t);
    return (sStack===tStack) ?  true : false;
    
};

 function getStackOf(string){
    let stack = [];
    string.split("").forEach((char)=>{
        if(char==="#"){
            stack.pop();
        }else{
            stack.push(char);
        } 
    });
    return stack.join("");
 }


 backspaceCompare("ab#c","ad#c")