/**
 * @param {string[]} logs
 * @return {number}
 */
var minOperations = function(logs) {
    let stack = [];
    let top = -1;
    let negatives = 0;
    logs.forEach((operation)=>{
        if(operation==="../"){
            if(top>-1){
                stack.pop();
                top--;
            }else{
                negatives--;
            }
            
        }else if(operation==="./"){

        }else{
            stack.push(operation);
            top++;
        }

    });
    return stack.length===0?stack.length:(negatives<0?-1*negatives:0);
};

minOperations(["d1/","../","../","../"]);