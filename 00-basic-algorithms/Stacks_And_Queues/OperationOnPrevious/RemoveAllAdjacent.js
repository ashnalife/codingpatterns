/**
 * @param {string} s
 * @return {string}
 */
var removeDuplicates = function(s) {
    let stack = [];
    let top = -1;
    s.split("").forEach((char)=>{
        if(char!==stack[top]){
            stack.push(char);
            top++;
        }else{
             stack.pop();
        top--;
        }
       
    });
    return stack.join("");
};