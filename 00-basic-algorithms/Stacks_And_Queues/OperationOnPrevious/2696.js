/**
 * @param {string} s
 * @return {number}
 */
var minLength = function(s) {
    let stack = [];
    let top =-1;
    s.split("").forEach((char)=>{
        if(top ===-1){
            stack.push(char);
            top++;
        }else if(char==="B" && stack[top]==="A"){
            stack.pop();
            top--;
        }else if(char==="D" && stack[top]==="C"){
            stack.pop();
            top--;
        }else{
            stack.push(char);
            top++;
        }
    });

    return stack.length;
};

let t = minLength("BQHSABE");
console.log(t);