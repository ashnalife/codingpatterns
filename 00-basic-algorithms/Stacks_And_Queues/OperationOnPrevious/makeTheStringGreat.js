/**
 * @param {string} s
 * @return {string}
 */
var makeGood = function(s) {
    let stack = [];
    let top = -1;
    s.split("").forEach((char)=>{
        if(top==-1){
            stack.push(char);
            top++;
        }else if(char.charCodeAt(0)>=56 && char.charCodeAt(0) <= 90 && char.toLowerCase()===stack[top]){
            stack.pop();
            top--;
        }else if(char.charCodeAt(0)>=97 && char.charCodeAt(0) <= 122 && char.toUpperCase()===stack[top]){
            stack.pop();
            top--;
        }else{
            stack.push(char);
            top++;
        }
    });
    return stack.join("");
};

makeGood("Pp");