var isValid = function(str) {
    let s=str.split("");
    let stack = [];
    let top = 0;
    for(let i=0;i<s.length;i++){
        if(s[i]==="(" || s[i]==="{" || s[i]==="["){
            stack.push(s[i]);
            top++;
        }else{
            if(stack.length=== 0){
                return false;
            }else{
                if(s[i] === ")" && stack[top-1]==="(" ){
                   stack.pop();
                   top--;
                }
                else if(s[i] === "}" && stack[top-1]==="{" ){
                   stack.pop();
                   top--;
                }
                else if(s[i] === "]" && stack[top-1]==="[" ){
                   stack.pop();
                   top--;
                }else{
                    return false;
                }
            }
        }
    }
    return stack.length ? false : true;
};
