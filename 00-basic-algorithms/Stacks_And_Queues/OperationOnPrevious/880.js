// https://leetcode.com/problems/decoded-string-at-index/

/**
 * @param {string} s
 * @param {number} k
 * @return {string}
 */
var decodeAtIndex = function (s, k) {
  let string = "";
  for(let i = 0; i<s.length; i++){
    let char = s[i];
    if (char.charCodeAt() >= 50 && char.charCodeAt() <= 57) {
        let lengthAfterExpansion = string.length * Number(char);
      if(k>=lengthAfterExpansion){
        string = repeatStackTimes(string, Number(char));
      }else if (k<string.length){
        return string.charAt(k-1);
      }else if(k>string.length && k<lengthAfterExpansion && restElementsAreOnlyNumber(s,i)[0]){
        if(restElementsAreOnlyNumber(s,i)[0]){
            let no = string.length;
            restElementsAreOnlyNumber(s,i)[1].map((mul)=>{
                no*=mul;
            });

            return string.charAt((no-1+string.length)%string.length);
            
        }
        return string.charAt((k-1+string.length)%string.length);
      }
    } else {
        string = string+char;
    }
  }
  return string.charAt(k-1);
};

function repeatStackTimes  (string, times) {
    let start = 0;
    let end  = string.length-1;
    let ns = ""
    while(times>0){
        for(let i = start;i<=end;i++){
            ns = ns + string.charAt(i);
        }
        times--;
    }
    return ns;
};

function restElementsAreOnlyNumber(s,i){
    let k = [];
    for(let j = i;j<s.length;j++){
        if (s[j].charCodeAt() < 50 && s[j].charCodeAt() > 57){
            return [false,null];
        }else{
            k.push(Number(s[i]));
        }
    }
    return [true,k];
}


console.log(decodeAtIndex("y959q969u3hb22odq595",222280369));
