/**
 * @param {string[]} operations
 * @return {number}
 */
var calPoints = function(operations) {
    let stack = [];
    let top =0;
    operations.forEach((operation)=>{
        if(operation==='+'){
            stack.push(stack[top-1]+stack[top-2]);
            top++;
        }else if(operation==='C'){
            stack.pop();
            top--;
        }else if(operation==='D'){
            stack.push(stack[top-1]*2);
            top++;
        }else{
            stack.push(Number(operation));
            top++;
        }
    });
    let result = 0;
    stack.forEach((item)=>{
        result+=item;
    });
    return result;
};

console.log(calPoints(["5","2","C","D","+"]));