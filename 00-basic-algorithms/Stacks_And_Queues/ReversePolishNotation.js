//statement :- https://leetcode.com/problems/evaluate-reverse-polish-notation/

/**
 * @param {string[]} tokens
 * @return {number}
 */
var evalRPN = function(tokens) {
    let stack = [];
    for(let i = 0; i<tokens.length; i++){
        let locAns;
        if(tokens[i]==="+"||tokens[i]==="*"||tokens[i]==="/"||tokens[i]==="-"){
            let right = stack.pop();
            let left = stack.pop();
            if(tokens[i]==="+"){
                locAns = left+right;
            }else if(tokens[i]==="/"){
                locAns = parseInt(left/right);
            }else if(tokens[i]==="*"){
                locAns = right*left;
            }else{
                locAns = left-right;
            }
        }
        if(locAns){
            stack.push(locAns);
        }else{
            stack.push(Number(tokens[i]));
        }
    }
    return stack.pop();
};


console.log(evalRPN(["10","6","9","3","+","-11","*","/","*","17","+","5","+"]));
