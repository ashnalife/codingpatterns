/**
 * @param {number[]} students
 * @param {number[]} sandwiches 
 * @return {number}
 */
var countStudents = function(students, sandwiches) {
    let dequeStudents = [...students];
    //here I am remapping methods of JS from standered deque.
    

    let attempt = 0;

    while(dequeStudents.length && attempt<=sandwiches.length){
        let currentStudent = dequeStudents.shift();
        if(currentStudent===sandwiches[0]){
            sandwiches.shift();
            attempt = 0;
        }else{
            dequeStudents.push(currentStudent)
            attempt++;
        }
    }

    return dequeStudents.length;

};

console.log(countStudents([1,1,1,0,0,1],[1,0,0,0,1,1]));