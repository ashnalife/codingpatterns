/**
 * @param {number[]} nums
 * @return {number[]}
 */
var nextGreaterElements = function(nums) {
    let result = new Array(nums.length).fill(-1);
    nums = [...nums,...nums];
    let stack = [];
    let top = -1;
    
    for(let i = 0; i<nums.length; i++){
        if(top===-1){
            stack.push(i);
            top++;
        }else{
            if(nums[i]>nums[stack[top]]){
                while(top>-1 && nums[i]>nums[stack[top]]){
                    result[stack[top]] = nums[i];
                    stack.pop();
                    top--;
                }
                
            }
            if(i<result.length){
                stack.push(i);
                    top++;
            }
            
        }
    }
    return result;
};

console.log(nextGreaterElements([1,2,3,4,3]));