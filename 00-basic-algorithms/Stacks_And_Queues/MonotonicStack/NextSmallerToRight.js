/**
 * 
 * @param {[Number]} array 
 */
function nextSmallerToRight(array){
    let result = new Array(array.length).fill(-1);
    let stack = [];
    let top = -1;
    //because we have to find values in future runner will move from right to left.
    for(let i = array.length-1; i>=0; i--){
        if(top === -1){
            stack.push(array[i]);
            top++;
        }else{
            //matlab stack me kuch to he check karege kya he

            if(stack[top]<array[i]){
                result[i] = stack[top];
            }else{
                while(stack[top]>array[i]){
                    stack.pop();
                    top--;
                }
                if(top>-1){
                    result[i] = stack[top];
                }
            }
            stack.push(array[i]);
            top++;
        }
    }
    return result;
}

console.log(nextSmallerToRight([2, 5, 9 ,3 ,1, 12 ,6 ,8 ,7]));

//[2, 5, 9 ,3 ,1, 12 ,6 ,8 ,7]
//[1,3,3,1,-1,6,-1,7,-1]