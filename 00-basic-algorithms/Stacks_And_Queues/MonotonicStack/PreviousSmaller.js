//https://www.interviewbit.com/problems/nearest-smaller-element/

/**
 * 
 * @param {[Number]} array 
 * @returns {[Number]}
 */
function previousSmaller(array){
    let stack = [];
    let top = -1;

    let result = new Array(array.length).fill(-1);
    for(let i = array.length-1; i>=0; i--){
        if(top===-1){
            stack.push(i);
            top++;
        }else{
            if(array[stack[top]]<array[i]){
                stack.push(i);
                top++;
            }else{
                while(array[stack[top]]>array[i] && top>-1){
                    result[stack[top]] = array[i];
                    stack.pop();
                    top--;
                }
                stack.push(i);
                top++;
            }
        }
    }
    return result;
}
console.log(previousSmaller([ 34, 35, 27, 42, 5, 28, 39, 20, 28 ]));
//-1 34 -1 27 -1 5 28 5 20 