/**
 *
 * @param {[Number]} array
 * @returns {[Number]}
 */
function nextGreaterToRight(array) {
  //Result initialise karo -1 se  
  let result = new Array(array.length).fill(-1);
  let stack = [];
  let top = -1;

  //because hame next value greater chahie to future me to ja ni sakte O(n) me
  //to ham left se move karege
  for(let i = array.length-1; i >= 0; i--){
    if(top===-1){
        stack.push(array[i]);
        top++;
    }else{
        //Agar stack ke top me badi value he matlab next greater jo already visited he
        if(stack[top]>array[i]){
            result[i] = stack[top];
        }else{
            //Agar stack ke top me value choti he to ane vali values ke lie current value badi hogi
            //magar current value se badi value stack me khojni padegi.
            while(top>-1 && stack[top]<array[i]){
                stack.pop();
                top--;
            }
            if(top>-1){
                result[i] = stack[top];
            }
        }
        //har ek value stack me to jaegi hi hame nahi pata ki aane vale value ke lie immidiate 
        //bata hoga ya nahi to better ki ham cache kare.
        stack.push(array[i]);
        top++;
    }
  }
  return result;
}

console.log(nextGreaterToRight([2,5,9,3,1,12,6,8,7]));

// ip [2,5,9,3,1,12,6,8,7]