/**
 * @param {[Number]} array
 * @returns {[Numbers]}
 */
function nextGreaterToLeft(array){
    let result = new Array(array.length).fill(-1);
    let stack = [];
    let top = -1;

    //because we have to find next greater in left we have to cache values from left
    //so loop will move toward right
    //or runner will go to right and will try to remember visited element

    for(let i = 0; i < array.length; i++){
        if(top === -1){
            stack.push(array[i]);
            top++;
        }else{
            if(stack[top]>array[i]){
                result[i] = stack[top];
            }else{
                while(stack[top]<array[i]){
                    stack.pop();
                    top--;
                }
                if(top>-1){
                    result[i] = stack[top];
                }
            }
            stack.push(array[i]);
            top++;
        }
    }
    return result;
};

console.log(nextGreaterToLeft([2 ,5 ,9 ,3 ,1 ,12, 6 ,8 ,7]));

