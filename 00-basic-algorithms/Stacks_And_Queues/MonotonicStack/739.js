/**
 * @param {number[]} temperatures
 * @return {number[]}
 */
var dailyTemperatures = function(temperatures) {
    //next minimum variation
    let stack = [];
    let result = new Array(temperatures.length).fill(0);
    let top = -1;

    for(let i = 0; i<temperatures.length; i++){
        if(top ===-1){
            stack.push(i);
            top++;
        }else{
            if(temperatures[stack[top]]<temperatures[i]){
                while(top>-1 && temperatures[stack[top]]<temperatures[i]){
                    result[stack[top]] = i-stack[top];
                    stack.pop();
                    top--;
                }
            }
            stack.push(i);
            top++;
        }
    }

    return result;
};

console.log(dailyTemperatures([73,74,75,71,69,72,76,73]));
//[1,1,4,2,1,1,0,0]