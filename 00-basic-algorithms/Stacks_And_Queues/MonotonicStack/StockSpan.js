/**
 * 
 * @param {[Number]} array 
 */
function stockSpan(array){

    let stack = [];
    let top = -1;
    let result = new Array(array.length).fill(1);

    //we have to to do operation in past or we need to remember element from left 

    //so here runner should move to right

    for(let i = 0; i<array.length; i++){
        if(top===-1){
            stack.push(i);
            top++;
        }else{
            if(array[stack[top]]>array[i]){
                
            }else{
                while(array[stack[top]]<array[i]){
                    let previousMin = stack.pop();
                    top--;
                    result[i] += result[previousMin];
                }
            }
            stack.push(i);
            top++;
        }
    }
    return result;
}

console.log(stockSpan([100, 80, 60, 70, 60, 75, 85]));

// ip [100, 80, 60, 70, 60, 75, 85]
//op  [1,   1,   1,  2,  1,  4,  6]