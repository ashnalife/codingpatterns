
var StockSpanner = function() {
    this.stack = [];
    this.top = -1;
    this.result = new Array();
    this.arr = [];
};

/** 
 * @param {number} price
 * @return {number}
 */
StockSpanner.prototype.next = function(item){

    this.arr.push(item);
    this.result.push(1);
    var array = this.arr;


    //we have to to do operation in past or we need to remember element from left 

    //so here runner should move to right

    for(let i = 0; i<array.length; i++){
        if(this.top===-1){
            this.stack.push(i);
            this.top++;
        }else{
            if(array[this.stack[this.top]]>array[i]){
                
            }else{
                while(array[this.stack[this.top]]<array[i]){
                    let previousMin = this.stack.pop();
                    this.top--;
                    this.result[i] += this.result[previousMin];
                }
            }
            this.stack.push(i);
            this.top++;
        }
    }
    return this.result[this.result.length-1];
};


/** 
 * Your StockSpanner object will be instantiated and called as such:
 * var obj = new StockSpanner()
 * var param_1 = obj.next(price)
 */

var stockSpanner = new StockSpanner();
console.log(stockSpanner.next(100)); // return 1
console.log(stockSpanner.next(80));  // return 1
console.log(stockSpanner.next(60));  // return 1
console.log(stockSpanner.next(70));  // return 2
console.log(stockSpanner.next(60));  // return 1
console.log(stockSpanner.next(75));  // return 4, because the last 4 prices (including today's price of 75) were less than or equal to today's price.
console.log(stockSpanner.next(85));  // return 6