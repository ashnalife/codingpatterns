/**
 * @param {number[][]} properties
 * @return {number}
 */
var numberOfWeakCharacters = function(properties) {

    let stack = [];
    let top = -1;
    let weakChar = 0;
    for(let i = 0; i<properties.length; i++){
        if(top===-1){
            stack.push(properties[i]);
            top++;
        }else{

            if(top>-1 && stack[top][0] < properties[i][0] && stack[top][1] < properties[i][1]){
                //incoming character is strong
                while(top>-1 && (stack[top][0] < properties[i][0] && stack[top][1] < properties[i][1])){
                    weakChar++;
                    stack.pop();
                    top--;
                }
                // stack.push(properties[i]);
                // top++;
            }else if(top>-1 && (stack[top][0] > properties[i][0] && stack[top][1] > properties[i][1])){
                //in coming character is weak
                weakChar++;
            }else{
                //incoming character is neither weak nor strong.
                // stack.push(properties[i]);
                // top++;
            }
            
        }
    }
    return weakChar;
};

console.log(numberOfWeakCharacters([[1,1],[2,1],[2,2],[1,2]]));