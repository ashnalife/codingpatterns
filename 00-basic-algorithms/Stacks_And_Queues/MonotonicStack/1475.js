//https://leetcode.com/problems/final-prices-with-a-special-discount-in-a-shop/
/**
 * @param {number[]} prices
 * @return {number[]}
 */
var finalPrices = function(prices) {
    //next minimum variation.
    let result = [...prices];//create copy of prices array
    let stack = [];
    let top = -1;

    for(let i = 0; i < prices.length; i++){
        if(top===-1){
            stack.push(i);
            top++;
        }else{
            if(prices[i]<=prices[stack[top]]){
                while(top>-1 && prices[i]<=prices[stack[top]]){
                    result[stack[top]] = result[stack[top]]-prices[i];
                    stack.pop();
                    top--;
                }
            }
                stack.push(i);
                top++;
            
        }
    }
    return result;
};

console.log(finalPrices([10,1,1,6]));