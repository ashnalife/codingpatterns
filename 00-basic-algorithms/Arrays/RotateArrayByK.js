/**
 * 
 * @param {[number]} array 
 * @param {number} k 
 */
function rotateArrayByK(array,k){
    
    while(k>array.length){
        k=k%array.length;
    }
    let result = new Array(array.length).fill(-Infinity);
    for(let i = 0 ;i<array.length;i++){
        if(i+k>=array.length){
            result[i] = array[i-array.length+k];
        }else{
            result[i]=array[i+k];
        }
    }
  
    return result;
}

console.log(rotateArrayByK([ 14, 5, 14, 34, 42, 63, 17, 25, 39, 61, 97, 55, 33, 96, 62, 32, 98, 77, 35 ],5));