/**
 * This sort function will sort an unsorted array in non-decreasing order
 * It moves from left to right and moves the current element to its correct position on the left
 * @param {number[]} array 
 */
function insertionSortI(array) {
    for (let i = 1; i < array.length; i++) {
        let ce = array[i];
        putCEAtRightPlace(array, i);
    }
}

/**
 * this function with put i that is current element at right place
 * @param {number[]} array 
 * @param {number} i 
 * @param {number} key 
 */
function putCEAtRightPlace(array, ce) {
   for(let i=ce; i>0; i--){
        if(array[i]<array[i-1]){
            swap(array,i-1,i);
        }else{
            break;
        }
   }
}

/**
 * Swaps two elements in an array.
 * @param {number[]} array - The array containing elements to be swapped.
 * @param {number} left - The index of the first element to be swapped.
 * @param {number} right - The index of the second element to be swapped.
 */
function swap(array, left, right) {
    let temp = array[right];
    array[right] = array[left];
    array[left] = temp;
}

let arr = [9, 7, 6, 5, 4, 3, 2, 9];
insertionSortI(arr);
console.log(arr);
