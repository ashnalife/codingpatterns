/**
 * Sorts an array using the selection sort algorithm.
 * It swaps local min element to left most element of subArray
 * @param {number[]} array - The array to be sorted.
 */
function selectionSortI(array) {
    for (let i = 0; i < array.length; i++) {
        // Find the index of the minimum element in the unsorted part of the array
        let localMinIndex = findLocalMinimum(array, i);
        
        // Swap the current element with the minimum element
        swap(array, i, localMinIndex);
    }
}

/**
 * Sorts an array using the selection sort algorithm.
 * It swaps local max element to right most element of subArray
 * @param {number[]} array - The array to be sorted.
 */
function selectionSortII(array) {
    for (let i = array.length-1; i >=0; i--) {
        // Find the index of the maximum element in the unsorted part of the array
        let localMaxIndex = findLocalMaximum(array, i);
        
        // Swap the current element with the minimum element
        swap(array, i, localMaxIndex);
    }
}

/**
 * Swaps two elements in an array.
 * @param {number[]} array - The array containing elements to be swapped.
 * @param {number} left - The index of the first element to be swapped.
 * @param {number} right - The index of the second element to be swapped.
 */
function swap(array, left, right) {
    let temp = array[right];
    array[right] = array[left];
    array[left] = temp;
}

/**
 * Finds the index of the minimum element in a subarray.
 * @param {number[]} array - The array in which to find the minimum element.
 * @param {number} index - The starting index of the subarray.
 * @returns {number} - The index of the minimum element.
 */
function findLocalMinimum(array, index) {
    let localMinIndex = index;
    for (let j = index; j < array.length; j++) {
        if (array[j] < array[localMinIndex]) {
            localMinIndex = j;
        }
    }

    return localMinIndex;
}

/**
 * Finds the index of the maximum element in a subarray.
 * @param {number[]} array - The array in which to find the maximum element.
 * @param {number} index - The last index of the subarray.
 * @returns {number} - The index of the maximum element.
 */
function findLocalMaximum(array, index) {
    let localMaxIndex = index;
    for (let j = index; j >=0; j--) {
        if (array[j] > array[localMaxIndex]) {
            localMaxIndex = j;
        }
    }

    return localMaxIndex;
}

let arr = [9,7,6,5,4,3,2,9];
selectionSortI(arr);
console.log(arr)