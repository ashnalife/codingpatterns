//https://leetcode.com/problems/first-unique-character-in-a-string/submissions/

var firstUniqChar = function(s) {
    
    //insertion order preserved Karna he

    let map = {};

    s.split("").forEach((char,index)=>{
        if(map[char]===undefined){
            map[char]=[index,0];
        }
        map[char][1]++;
    })

    let fnr = Infinity;
    console.log(map);
    Object.keys(map).forEach((char)=>{
        if(map[char][1]===1){
            fnr = Math.min(fnr,map[char][0]);
        }
    })
    return fnr==Infinity?-1:fnr;
};