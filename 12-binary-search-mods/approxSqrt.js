//can be solved in one liner using Math.floor(Math.sqrt(x)) complexity unknown
//can be solved by looping from a to x/2 and check of i*i==x O(n)
//this function uses Binary search for O(log2X) soln

var mySqrt = function(x) {
    if(x < 2) return x;
    let lo = 1;
    let hi = Math.floor(x/2);
    let mid;
    let result = 0;
    while(lo<=hi){
        mid = Math.floor((lo+hi)/2);
        let sqr = mid*mid;
        if(sqr === x){
            return mid;
        }else if(sqr > x){
            hi=mid-1;
        }else if(sqr < x){
            lo=mid+1;
            //cache kar lena he last mid ko
            // before moving jockey to right or 
            //changing the search space where it could cross the desired result
            result=mid;
        }
    }
return result;
};

