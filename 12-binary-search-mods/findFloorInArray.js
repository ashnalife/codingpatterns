//https://www.geeksforgeeks.org/problems/floor-in-a-sorted-array-1587115620/1

// Example 1:

// Input:
// N = 7, x = 0
// arr[] = {1,2,8,10,11,12,19}
// Output: -1
// Explanation: No element less
// than 0 is found. So output
// is "-1".

// Example 2:

// Input:
// N = 7, x = 5
// arr[] = {1,2,8,10,11,12,19}
// Output: 1
// Explanation: Largest Number less than 5 is
// 2 (i.e K = 2), whose index is 1(0-based
// indexing).

function getFloorValue(array, target) {
  //your code here
  let left = 0;
  let right = array.length - 1;
  let mid;
  while (left <= right) {
    mid = Math.floor((left + right) / 2);

    //If you came till {{{last to be searched}}} index that means this could be floor value
    if (left == right) {
      if (array[left] > target) {
        return -1;
      } else {
        return left;
      }
    }
    //move left half and right half typical BS
    if (array[mid] === target) {
      return mid;
    } else if (array[mid] > target) {
      right = mid - 1;
    } else if (array[mid] < target) {
      left = mid + 1;
    } else {
      return -1;
    }
  }

  return -1;
}

console.log(getFloorValue([1,2,8,10,11,12,19],0));
console.log(getFloorValue([1,2,8,10,11,12,19],9));
console.log(
  getFloorValue(
    [
      64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81,
      82, 83, 84, 85, 86, 87, 88, 89, 90, 91,
    ],
    9
  )
);

//there is one similar problem called ceil value of a sorted array.
