//leetcode
//744. 

/**
 * @param {character[]} letters
 * @param {character} target
 * @return {character}
 */
var nextGreatestLetter = function(letters, target) {
    let left = 0;
    let right = letters.length-1;
    let mid;
    while(left<=right){
        mid = Math.floor((left+right)/2);

        if(left===right){
            return letters[left]>target?letters[left]:letters[left===letters.length-1?0:left+1];
        }

        if(letters[mid]===target){
            //if target found
            return mid===letters.length-1?letters[0]:getNextChar(letters,mid,target);
        }else if(letters[mid]>target){
            right=mid-1;
        }else if(letters[mid]<target){
            left=mid+1;
        }
    }
    return letters[0];
};

function getNextChar(letters,mid,target){
    let next = letters[mid+1];
    if(next>target){
        return next;
    }
    for(let i=mid+1;i<letters.length;i++){
        if(letters[i]>next){
            next=letters[i];
            break;
        }
    }
    return next===target?letters[0]:next;
}

nextGreatestLetter(["a","b","c","d","e","f","g","h","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"],"a");
