var findMin = function (nums) {
    // A minimum number is pivot point of which both side is sorted
    //or it is at index which has been shifted to right
  
    ////[1,2,4,5,6,7,8,9,0],0
    // we can easily do this by linear search in O(n) time but here the demand
    //is to perform in O(logn)
   if(nums.length===1) return nums[0];
   if(nums.length===2) return nums[0]<nums[1]?nums[0]:nums[1];
    let left = 0;
    let right = nums.length - 1;
  
    let mid;
  
    //we need to check which side we have to go ,only then we can ensure O(logn)
    //basically we need to divide
  
    while (left <= right) {
      mid = Math.floor((left + right) / 2)
      //if mid is minimum
      if (isMinimumCircular(nums, mid)) {
        return nums[mid];
      }
      //else which half to look decide but on which ground
      //pivot is minimum and and desorting begins from that point
      //we need to move toward unsorted half
      else if (!isSortedNonDec(nums, left, mid)) {
        right = mid - 1;
      } else if (!isSortedNonDec(nums, mid+1,right)) {
        left = mid + 1;
      }else{
          //it means both sides are sorted then we will move which ever adjacent is minimum
          const [prev, next] = getPrevNext(nums, mid);
          if(nums[prev]<nums[next]){
              right = mid - 1;  
          }else{
              left = mid + 1;
          }
  
      }
    }
  };
  
  function isMinimumCircular(nums, mid) {
    const [prev, next] = getPrevNext(nums, mid);
    //if it is less than both left and right that too in circular we can say its min
    //this if else is redundant but improves redability
    if (nums[mid] < nums[prev] && nums[mid] < nums[next]) {
      return true;
    } else {
      return false;
    }
  }
  
  function getPrevNext(nums, mid) {
    let n = nums.length;
    //protect index out of bound for negative index circular array logic
    //Circular Array
    let prev = (mid - 1 + n) % n;
    let next = (mid + 1) % n;
    return [prev, next];
  }
  
  function isSortedNonDec(nums, left, right) {
    return nums[left] <= nums[right];
  }
  