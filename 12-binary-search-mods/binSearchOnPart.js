/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var binSearch = function (nums, target,left,right) {
    let mid;
  
    while (left <= right) {
      mid = Math.floor((left + right) / 2);
      if (nums[mid] === target) {
        return mid;
      } else if (nums[mid] > target) {
        right = mid - 1;
      } else {
        left = mid + 1;
      }
    }
  
    return -1;
  };