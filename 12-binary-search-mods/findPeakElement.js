//leetcode
//BSA [Binary Search on Answer][Binary Search on unsorted array]
//162. Find Peak Element
//You must write an algorithm that runs in O(log n) time.

/**
 * @param {number[]} nums
 * @return {number}
 */
var findPeakElement = function(nums) {
    if(nums.length===1) return 0;
    let left = 0;
    let right = nums.length-1;
    let mid;
    while(left<=right){
        mid=Math.floor((left+right)/2);
        if(left===right && !checkIfPeak(nums,mid)){
            //this case is when we are unable to find any peak.
            return -1;
        }
        if(checkIfPeak(nums,mid)){
            return mid;
        }else if(getWhichDirectionToGo(nums,mid)==="left"){
            right=mid-1;
        }else{
            left=mid+1;
        }
    }
    return -1;
};

var checkIfPeak = (nums,mid) => {
    //here I am expecting nums to be of length atleat 2
    if(mid===0){
        if(nums[mid]>nums[mid+1]) return true;
    }else if(mid===nums.length-1){
        if(nums[mid]>nums[mid-1]) return true;
    }else{
        if(nums[mid]>nums[mid-1] && nums[mid]>nums[mid+1]) return true;
    }
    return false;
};

var getWhichDirectionToGo =(nums,mid) => {
    
    if(nums[mid]<nums[mid-1]) return 'left';
  
    return 'right';
}

console.log(findPeakElement([1,2,1,3,5,6,4]));
console.log(findPeakElement([1,2,3,1]));