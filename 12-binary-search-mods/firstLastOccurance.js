//Modified Binary Search.
function getFirstAndLastOccurance(nums, target){
    return [getFirstOccurance(nums, target),getLastOccurance(nums, target)];
}
/**
 * 
 * @param {[]number} nums 
 * @param {number} target 
 */
const getFirstOccurance = (nums, target) => {
    let lo = 0;
    let hi = nums.length-1;
    let firstOccurance = -1;
    let mid;
    while(lo<=hi){
        mid = Math.floor((hi+lo)/2);
        //console.log("1",mid,lo,hi);
        if(nums[mid]===target){
            firstOccurance = mid;
            lo=0;
            hi = mid-1;
        }else if(nums[mid]>target){
            hi=mid-1;
        }else if(nums[mid]<target){
            lo=mid+1;
        }else if(lo==hi){
            break;
        }
    }
    return firstOccurance;
};

const getLastOccurance = (nums, target) => {
    let lo = 0;
    let hi = nums.length-1;
    let lastOccurance = -1;
    let mid;
    while(lo<=hi){
        mid = Math.floor((hi+lo)/2);
        //console.log("1",mid,lo,hi);
        if(nums[mid]===target){
            lastOccurance = mid;
            lo=mid+1;
            hi = nums.length-1;
        }else if(nums[mid]>target){
            hi=mid-1;
        }else if(nums[mid]<target){
            lo=mid+1;
        }else if(lo==hi){
            break;
        }
    }
    return lastOccurance;
};

const findNoOfOccurancesInSortedArray = (array,target) => {
    //not optimised for edge cases.
    let [first,last] = getFirstAndLastOccurance(array,target);
    return last-first+1;
};

console.log(findNoOfOccurancesInSortedArray([5,7,7,7,7,7,7,7,7,7,7,8,8,8,10],8));

//O(n) solution is easy and it can be done using linear search logic
//O(logn) solution can be done using binary search