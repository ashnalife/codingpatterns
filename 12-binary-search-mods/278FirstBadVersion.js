/**
 * Definition for isBadVersion()
 *
 * @param {integer} version number
 * @return {boolean} whether the version is bad
 * **/
const isBadVersion = function (version) {
  return version === 4;
};

/**
 * @param {function} isBadVersion()
 * @return {function}
 */
var solution = function (isBadVersion) {
  /**
   * @param {integer} n Total versions
   * @return {integer} The first bad version
   */
  return function (n) {
    let left = 1;
    let right = n;
    let mid;
    while (left <= right) {
      mid = left + Math.floor((right - left) / 2);
      if (left === right) {
        if (isBadVersion(left)) {
          return left;
        } else {
          return -1;
        }
      }
      if (isBadVersion(mid)) {
        if (mid === 0) {
          return mid;
        } else {
          if (isBadVersion(mid - 1)) {
            right = mid - 1;
          } else {
            return mid;
          }
        }
      } else {
        left = mid + 1;
      }
    }
  };
};

console.log(solution(isBadVersion)(5));
