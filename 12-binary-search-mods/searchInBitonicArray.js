// a non decreasing sorted array is a nonotonic array it will keep increasing
// a bitonic array is increading upto some index and then it will decrease.

//https://www.interviewbit.com/problems/search-in-bitonic-array/
// Ensure O(logn) complexity
var searchBitonicArray = (nums, target) => {
  let indexOfPeak = findPeakElement(nums);
  let leftHalf = binSearchInc(nums, target, 0, indexOfPeak);
  if (leftHalf > -1) return leftHalf;
  if (indexOfPeak < nums.length - 1) {
    let rightHalf = binSearchDec(nums, target, indexOfPeak, nums.length - 1);
    if (rightHalf > -1) {
      return rightHalf;
    }
  }
  return -1;
};
// find peak element is the previous problem whose solution will be used here
var findPeakElement = function (nums) {
  if (nums.length === 1) return 0;
  let left = 0;
  let right = nums.length - 1;
  let mid;
  while (left <= right) {
    mid = Math.floor((left + right) / 2);
    if (left === right && !checkIfPeak(nums, mid)) {
      //this case is when we are unable to find any peak.
      return -1;
    }
    if (checkIfPeak(nums, mid)) {
      return mid;
    } else if (getWhichDirectionToGo(nums, mid) === "left") {
      right = mid - 1;
    } else {
      left = mid + 1;
    }
  }
  return -1;
};

var checkIfPeak = (nums, mid) => {
  //here I am expecting nums to be of length atleat 2
  if (mid === 0) {
    if (nums[mid] > nums[mid + 1]) return true;
  } else if (mid === nums.length - 1) {
    if (nums[mid] > nums[mid - 1]) return true;
  } else {
    if (nums[mid] > nums[mid - 1] && nums[mid] > nums[mid + 1]) return true;
  }
  return false;
};

var getWhichDirectionToGo = (nums, mid) => {
  if (nums[mid] < nums[mid - 1]) return "left";

  return "right";
};

var binSearchInc = function (nums, target, left, right) {
  let mid;

  while (left <= right) {
    mid = Math.floor((left + right) / 2);
    if (nums[mid] === target) {
      return mid;
    } else if (nums[mid] > target) {
      right = mid - 1;
    } else {
      left = mid + 1;
    }
  }

  return -1;
};

var binSearchDec = function (nums, target, left, right) {
  let mid;

  while (left <= right) {
    mid = Math.floor((left + right) / 2);
    if (nums[mid] === target) {
      return mid;
    } else if (nums[mid] < target) {
      right = mid - 1;
    } else {
      left = mid + 1;
    }
  }

  return -1;
};
