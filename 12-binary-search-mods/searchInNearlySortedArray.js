//https://www.geeksforgeeks.org/search-almost-sorted-array/


function searchNSA (array,target){

    let left = 0;
    let right = array.length-1;
    let mid;
    while(left<=right){

        mid = Math.floor((left+right)/2);
        let res = checkForEqualityNearest(array,mid,target);
        if(res!==-1){
            return res;
        }else if(array[mid]>target){
            right = mid-1;
        }else if(array[mid]<target){
            left = mid+1;
        }
    }

    return -1;
}

function checkForEqualityNearest(array,mid,target){
    if(array[mid]===target){
        return mid;
    }else if(array[mid>0?mid-1:mid]===target){
        return mid>0?mid-1:mid;
    }else if(array[mid===array.length-1?mid:mid+1]===target){
        return mid===array.length-1?mid:mid+1;
    }else{
        return -1;
    }
}

console.log(searchNSA([10, 3, 40, 20, 50, 80, 70],40));
console.log(searchNSA([10, 3, 40, 20, 50, 80, 70],90));
console.log(searchNSA([3,1,2],3));