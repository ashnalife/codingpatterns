//https://leetcode.com/problems/single-element-in-a-sorted-array/

/**
 * @param {number[]} nums
 * @return {number}
 */
var singleNonDuplicate = function (nums) {
    //[3,3,7,7,10,11,11,12,12,13,13]
    //the trick here is to observe a pattern that after the unique element
    //twin element started appearing in right instead left.
    let left = 0;
    let right = nums.length - 1;
    let mid;
    while (left <= right) {
      mid = left + Math.floor((right - left) / 2);
  
      if (isUnique(nums,mid)) {
        return nums[mid];
      } else if (whichWayToGo(nums, mid) === "left") {
        right = mid - 1;
      } else {
        left = mid + 1;
      }
    }
  };
  
  const isUnique = (nums,mid) => {
      if(mid===0 && nums[mid]!==nums[mid+1]){
          return true;
      }else if(mid===nums.length-1 && nums[mid]!==nums[mid-1]){
          return true;
      }else if(nums[mid]!==nums[mid+1] && nums[mid]!==nums[mid-1]){
          return true;
      }
      return false;
  };
  
  const whichWayToGo = (nums, mid) => {
      if(mid%2===0){
          if(nums[mid]===nums[mid+1]){
              return "right";
          }else{
              return 'left';
          }
      }else{
          //odd
          if(nums[mid-1]===nums[mid]){
              return "right";
          }else{
              return 'left';
          }
      }
  };